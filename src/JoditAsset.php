<?php

namespace exoo\joditeditor;

/**
 * Asset bundle for widget [[JoditEditor]].
 */
class JoditAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/jodit/build';
    /**
     * @inheritdoc
     */
    public $css = [
        'jodit.min.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'jodit.min.js',
    ];
}
