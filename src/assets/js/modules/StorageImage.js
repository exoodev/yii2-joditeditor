Jodit.modules.StorageImage = function(editor) {
    var _this = this;
    var $body;

    this.options = {
        folderURL: '/storage/folder/index',
        filesURL: '/storage/folders',
        storageParser: '{{storage}}',
        storageParsing: false,
    }

    this.insert = function() {
        showModal();
    };
    this.init = function() {
        // editor.events
        //     .on('change afterInit', function (e) {
        //         //
        //     });
    };

    function showModal() {
        var template = [
            '<div class="uk-modal uk-modal-container">',
                '<div class="uk-modal-dialog uk-height-1-1">',
                    '<div class="uk-modal-body">',
                        '<button class="uk-modal-close-default" type="button" uk-close></button>',
                        '<h3>Изображения</h3>',
                        '<div class="ex-htmleditor-storage"></div>',
                    '</div>',
                '</div>',
            '</div>',
        ].join('');

        var dialog = UIkit.modal(template);
        dialog.show();

        getFolders();

        $(dialog.$el).on('hidden', function(e) {
            $(this).remove();
        });

        $body = $(dialog.$el)
            .find('.ex-htmleditor-storage')
            .on('click', '.ex-htmleditor-storage-folder', function(e) {
                e.preventDefault();
                getFiles($(this).data('item'));
            })
            .on('click', '.ex-htmleditor-storage-folders', function(e) {
                e.preventDefault();
                getFolders();
            })
            .on('click', '.ex-htmleditor-storage-file', function(e) {
                e.preventDefault();
                var src = $(this).data('storage');

                if (_this.options.storageParsing) {
                    src = _this.options.storageParser + src;
                }
                
                var image = editor.editorDocument.createElement('img');
                image.setAttribute('src', src + $(this).data('path'));
                editor.selection.insertNode(image);
                editor.setEditorValue();
                dialog.hide();
            });
    }

    function getFiles(folderId) {
        $.get(_this.options.filesURL + '/' + folderId + '?expand=files').done(function(data) {
            var items = [];

            $.each(data.files, function(index, file) {
                var date = new Date(file.created_at * 1000);
                var createdAt = date.toLocaleString();
                var parser = parseUrl(file.url);

                items.push([
                    '<div>',
                        '<a class="ex-htmleditor-storage-file" href="#" data-item="'+file.id+'" data-path="'+parser.pathname+'" data-storage="'+parser.origin+'">',
                            '<div class="uk-card uk-card-default uk-card-small uk-box-shadow-small uk-box-shadow-hover-medium uk-animation-scale-up">',
                                '<div class="uk-card-media-top">',
                                    '<div class="uk-cover-container uk-height-small">',
                                        '<img src="'+file.url+'" uk-cover>',
                                    '</div>',
                                '</div>',
                                '<div class="uk-card-footer uk-text-small" style="padding: 13px 10px;">',
                                    '<div class="uk-text-truncate uk-flex uk-flex-middle">',
                                        '<i class="far fa-calendar-alt fa-lg uk-margin-small-right uk-text-muted"></i>',
                                        '<span>'+createdAt+'</span>',
                                    '</div>',
                                '</div>',
                            '</div>',
                        '</a>',
                    '</div>',
                ].join(''));
            });

            $body.html([
                '<h4>',
                    '<a class="ex-htmleditor-storage-folders" href="#"><i class="far fa-images fa-lg"></i></a>',
                    '<i class="fas fa-angle-right fa-sm uk-text-muted uk-margin-left uk-margin-right"></i>',
                    '<span>'+data.name+'</span>',
                '</h4>',
                '<div class="uk-child-width-1-6@m uk-grid-small" uk-grid>',
                    items.join(''),
                '</div>',
            ].join(''));
        });
    }

    function getFolders() {
        $.get(_this.options.folderURL).done(function(data) {
            var items = [];

            $.each(data, function(index, folder) {
                items.push([
                    '<div>',
                        '<a class="ex-htmleditor-storage-folder" href="#" data-item="'+folder.id+'">',
                            '<div class="uk-card uk-card-default uk-card-small uk-box-shadow-small uk-box-shadow-hover-medium">',
                                '<div class="uk-card-header">',
                                    '<h3 class="uk-h5 uk-text-truncate uk-margin-top uk-margin-bottom uk-flex uk-flex-middle">',
                                        '<i class="fas fa-folder fa-2x uk-margin-small-right uk-text-muted"></i>',
                                        '<span>'+folder.name+'</span>',
                                    '</h3>',
                                '</div>',
                            '</div>',
                        '</a>',
                    '</div>',
                ].join(''));
            });

            $body.html([
                '<div class="uk-child-width-1-6@m uk-grid-small" uk-grid>',
                    items.join(''),
                '</div>',
            ].join(''));
        });
    }

    function parseUrl(url) {
        var a = document.createElement('a');
        a.href = url;
        return a;
    }
};

Jodit.defaultOptions.controls.storageImage = {
    exec: function (editor) {
        editor.getInstance('StorageImage').insert();
    },
    tooltip: 'Select image'
};

Jodit.lang.ru = $.extend({}, {
   'Select image': 'Выбрать изображение'
}, Jodit.lang.ru);
