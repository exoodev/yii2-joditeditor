Jodit.modules.Headings = function(editor) {
    var _this = this;
    var $body;

    this.insert = function() {
        showModal();
    };
    this.init = function() {
        // editor.events
        //     .on('change afterInit', function (e) {
        //         //
        //     });
    };
};

Jodit.defaultOptions.controls.Headings = {
    exec: function (editor) {
        editor.getInstance('Headings').insert();
    },
    tooltip: 'Headings'
};

Jodit.lang.ru = $.extend({}, {
   'Headings': 'Заголовки'
}, Jodit.lang.ru);
