var JoditEditor = {
    init: function(id, options) {

        var defaultOptions = {
            sourceEditorNativeOptions: {
                showGutter: false,
                theme: 'ace/theme/monokai',
            },
            askBeforePasteFromWord: false,
            askBeforePasteHTML: false,
            buttons: [
                'source', '|',
                'bold',
                'strikethrough',
                'underline',
                'italic', '|',
                'ul',
                'ol', 'align', '|',
                'outdent', 'indent',  '|',
                'paragraph', '|',
                'image',
                'storageImage',
                'video',
                'table',
                'link', '|',
                'undo', 'redo', '|',
                'hr',
                'eraser',
                'fullsize',
            ],
            buttonsMD: [
                'source', '|',
                'bold',
                'strikethrough',
                'underline',
                'italic', '|',
                'ul',
                'ol', 'align', '|',
                'outdent', 'indent',  '|',
                'paragraph', '|',
                'image',
                'storageImage',
                'video',
                'table',
                'link', '|',
                'undo', 'redo', '|',
                'hr',
                'eraser',
                'fullsize',
            ],
            buttonsXS: [
                'source', '|',
                'bold',
                'strikethrough',
                'underline',
                'italic', '|',
                'ul',
                'ol', 'align', '|',
                'outdent', 'indent',  '|',
                'paragraph', '|',
                'image',
                'storageImage',
                'video',
                'table',
                'link', '|',
                'undo', 'redo', '|',
                'hr',
                'eraser',
                'fullsize',
            ],
            events: {
                getIcon: function (name, control, clearName) {
                    var code = clearName;

                    switch (clearName) {
                        case 'undo':
                            code = 'undo-alt';
                            break;
                        case 'redo':
                            code = 'redo-alt';
                            break;
                        case 'copyformat':
                            code = 'clone';
                            break;
                        case 'about':
                            code = 'question';
                            break;
                        case 'selectall':
                            code = 'check-square';
                            break;
                        case 'symbol':
                            return '<span style="text-align: center;font-size:18px;font-weight: bold;">Ω</span>';
                        case 'fullsize':
                            code = 'expand-arrows-alt';
                            break;
                        case 'hr':
                            code = 'minus';
                            break;
                        case 'left':
                        case 'right':
                        case 'justify':
                        case 'center':
                            code = 'align-' + name;
                            break;
                        case 'brush':
                            code = 'fill-drip';
                            break;
                        case 'fontsize':
                            code = 'text-height';
                            break;
                        case 'ul':
                        case 'ol':
                            code = 'list-' + name;
                            break;
                        case 'source':
                            code = 'code';
                            break;
                        case 'storageImage':
                            code = 'images';
                            break;
                        case 'headings':
                            code = 'text-height';
                            break;
                        case 'valign':
                            code = 'arrows-alt-v';
                            break;
                        case 'pencil':
                            code = 'pencil-alt';
                            break;
                        case 'bin':
                            code = 'trash-alt';
                            break;
                        case 'dots':
                            code = 'ellipsis-v';
                            break;
                    }
                    return '<i class="fas fa-' + code + '"></i>';
                }
            }
        };

        options = $.extend(true, defaultOptions, options);

        this.editor = new Jodit(id, options);
        // init module
        // this.editor.getInstance('StorageImage').init();
    }
}
