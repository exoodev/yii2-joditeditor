<?php

namespace exoo\joditeditor;

/**
 * Asset bundle for widget [[JoditEditor]].
 */
class JoditEditorAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/joditeditor/assets';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/joditEditor.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'js/joditEditor.js',
        'js/modules/StorageImage.js',
        // 'js/modules/Headings.js',
    ];
}
