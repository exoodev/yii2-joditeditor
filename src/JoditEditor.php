<?php

namespace exoo\joditeditor;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * JoditEditor
 *
 * For example to use the timepicker with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo JoditEditor::widget([
 *     'model' => $model,
 *     'attribute' => 'text',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo JoditEditor::widget([
 *     'name'  => 'text',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'text')->widget(JoditEditor::classname(), [
 *     'clientOptions' => ['lineNumbers' => true],
 * ]) ?>
 * ```
 */
class JoditEditor extends InputWidget
{
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // $uikit = UikitAsset::register(Yii::$app->view);
        $defaults = [
            'language' => substr(Yii::$app->language, 0, 2),
            // 'beautifyHTML' => false,
            // 'cleanHTML' => [
            //     'removeEmptyElements' => false,
            //     'fillEmptyParagraph' => true,
            //     'replaceNBSP' => true,
            //     'cleanOnPaste' => true,
            //     'replaceOldTags' => false,
            // ],
            // 'askBeforePasteHTML' => false,
            // 'askBeforePasteFromWord' => false,
            // 'iframe' => true,
            // 'iframeBaseUrl' => Yii::$app->settings->get('system', 'frontendUrl'),
            // 'iframeCSSLinks' => [
            //     $uikit->baseUrl . '/css/uikit.min.css'
            // ],
        ];
        $clientOptions = Json::encode(array_replace_recursive($defaults, $this->clientOptions));
        $id = $this->options['id'];
        $view = $this->getView();
        $view->registerJs("JoditEditor.init('#$id', $clientOptions);");
        JoditAsset::register($view);
        JoditEditorAsset::register($view);
    }

    /**
     * @inheritdoc
     */
    public function run()
	{
        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            return Html::textarea($this->name, $this->value, $this->options);
        }
	}
}
